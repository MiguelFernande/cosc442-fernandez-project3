/**
 * 
 */
package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 */
public class VendingMachineTest {
	
	VendingMachine MyVendingMachine;
	VendingMachineItem item1;
	VendingMachineItem item2;


	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception 
	{
		MyVendingMachine = new VendingMachine();
		item1 = new VendingMachineItem("Tee Hees", 2.0);
        item2 = new VendingMachineItem("Bone-In Rib Eye", 2.0);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception 
	{
		MyVendingMachine = null;
	}

	//Tests adding an item to the vending machine.
	@Test
	public void testAddItem() 
	{
		MyVendingMachine.addItem(item1, VendingMachine.A_CODE);
		assertEquals(item1, MyVendingMachine.getItem(VendingMachine.A_CODE));
	}
	
	//Tests adding an item to an already occupied slot, expecting a VendingMachineException.
	@Test(expected = VendingMachineException.class)
	public void testAddItemToOccupiedSlot() throws VendingMachineException 
	{
	    VendingMachineItem item1 = new VendingMachineItem("Item 1", 1.0);
	    VendingMachineItem item2 = new VendingMachineItem("Item 2", 2.0);
	    
	    MyVendingMachine.addItem(item1, VendingMachine.A_CODE);
	    MyVendingMachine.addItem(item2, VendingMachine.A_CODE);
	}
    
    //Tests adding an item with an invalid code, expecting a VendingMachineException.
	@Test(expected = VendingMachineException.class)
	public void testAddItemWithInvalidCode() throws VendingMachineException 
	{
	    MyVendingMachine.addItem(item1, "InvalidCode");
	}

    //Tests removing an item from the vending machine.
    @Test
    public void testRemoveItem() throws VendingMachineException 
    {
        MyVendingMachine.addItem(item1, VendingMachine.A_CODE);
        VendingMachineItem removedItem = MyVendingMachine.removeItem(VendingMachine.A_CODE);
        assertEquals(item1, removedItem);
        assertNull(MyVendingMachine.getItem(VendingMachine.A_CODE));
    }
    
    //Tests removing an item from an empty slot, expecting a VendingMachineException.
    @Test(expected = VendingMachineException.class)
    public void testRemoveItemFromEmptySlot() throws VendingMachineException 
    {
    	MyVendingMachine.removeItem(VendingMachine.A_CODE);
    }
    
    //Tests removing an item with an invalid code, expecting a VendingMachineException.
    @Test(expected = VendingMachineException.class)
    public void testRemoveItemWithInvalidCode() throws VendingMachineException 
    {
    	MyVendingMachine.removeItem("InvalidCode");
    }

    //Tests inserting money into the vending machine.
    @Test
    public void testInsertMoney() throws VendingMachineException
    {
    	MyVendingMachine.insertMoney(10.0);
        assertEquals(10.0, MyVendingMachine.getBalance(), 0.001);
    }
    
    //Tests inserting a negative amount of money, expecting a VendingMachineException.
    @Test(expected = VendingMachineException.class)
    public void testInsertNegativeMoney() throws VendingMachineException 
    {
    	MyVendingMachine.insertMoney(-5.0);
    }
    
    //Tests getting the balance of the vending machine.
    @Test
    public void testGetBalance() throws VendingMachineException 
    {
    	MyVendingMachine.insertMoney(10.0);
        assertEquals(10.0, MyVendingMachine.getBalance(), 0.001);
    }

    //Tests making a purchase from the vending machine.
    @Test
    public void testMakePurchase() throws VendingMachineException 
    {
        MyVendingMachine.addItem(item1, VendingMachine.A_CODE);
        MyVendingMachine.insertMoney(2.0);
        assertTrue(MyVendingMachine.makePurchase(VendingMachine.A_CODE));
        assertEquals(0.0, MyVendingMachine.getBalance(), 0.001);
        assertNull(MyVendingMachine.getItem(VendingMachine.A_CODE));
    }
    
    //Tests making a purchase with insufficient balance.
    @Test
    public void testMakePurchaseWithInsufficientBalance() throws VendingMachineException 
    {
        MyVendingMachine.addItem(item1, VendingMachine.A_CODE);
        MyVendingMachine.insertMoney(1.0);
        assertFalse(MyVendingMachine.makePurchase(VendingMachine.A_CODE));
        assertEquals(1.0, MyVendingMachine.getBalance(), 0.001);
        assertEquals(item1, MyVendingMachine.getItem(VendingMachine.A_CODE));
    }
    
    //Tests making a purchase from an empty slot.
    @Test
    public void testMakePurchaseFromEmptySlot() throws VendingMachineException 
    {
    	MyVendingMachine.insertMoney(2.0);
        assertFalse(MyVendingMachine.makePurchase(VendingMachine.A_CODE));
        assertEquals(2.0, MyVendingMachine.getBalance(), 0.001);
    }

    //Tests returning change from the vending machine.
    @Test
    public void testReturnChange() throws VendingMachineException 
    {
    	MyVendingMachine.insertMoney(10.0);
        double change = MyVendingMachine.returnChange();
        assertEquals(10.0, change, 0.001);
        assertEquals(0.0, MyVendingMachine.getBalance(), 0.001);
    }
    
    @Test(expected = VendingMachineException.class)
    public void testAddItemWithSameName() throws VendingMachineException 
    {
        VendingMachineItem item1 = new VendingMachineItem("Tee Hees", 2.0);
        VendingMachineItem item2 = new VendingMachineItem("Tee Hees", 3.0);
        MyVendingMachine.addItem(item1, VendingMachine.A_CODE);
        MyVendingMachine.addItem(item2, VendingMachine.B_CODE);
    }
    
    @Test
    public void testIsItemAvailableWithInvalidCode() 
    {
        assertFalse(MyVendingMachine.isItemAvailable("InvalidCode"));
    }
    
    @Test
    public void testGetSlotIndexForCodeC() throws VendingMachineException 
    {
        int slotIndex = MyVendingMachine.getSlotIndex(VendingMachine.C_CODE);
        assertEquals(2, slotIndex);
    }
    
    @Test
    public void testGetSlotIndexForCodeD() throws VendingMachineException 
    {
        int slotIndex = MyVendingMachine.getSlotIndex(VendingMachine.D_CODE);
        assertEquals(3, slotIndex);
    }
    
    

}
