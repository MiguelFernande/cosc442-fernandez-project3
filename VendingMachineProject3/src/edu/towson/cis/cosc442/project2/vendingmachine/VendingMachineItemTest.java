/**
 * 
 */
package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import junit.framework.AssertionFailedError;

/**
 * 
 */
public class VendingMachineItemTest {
	
	VendingMachineItem VendingItem;
	VendingMachineItem FreeVendingItem;
	VendingMachineItem StolenVendingItem;


	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception 
	{
		VendingItem = new VendingMachineItem("Cheesy Doodles", 15.0);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception 
	{
		VendingItem = null;
	}

	//Tests creating a VendingMachineItem with a name and price.
	@Test
	public void testVendingMachineItem() 
	{
		assertEquals("Cheesy Doodles", VendingItem.getName());
		assertEquals(15.0, VendingItem.getPrice(), 0.001);
	}
	
	//Tests creating a VendingMachineItem with a price of zero.
	@Test
	public void testVendingMachineItemPriceZero() 
	{
		FreeVendingItem = new VendingMachineItem("Air", 0.0);
		assertEquals("Air", FreeVendingItem.getName());
		assertEquals(0.0, FreeVendingItem.getPrice(), 0.001);
	}
	
	//Tests creating a VendingMachineItem with a negative price, expecting a VendingMachineException.
	@Test
	public void testVendingMachineItemPriceNegative() 
	{
		try {
	        new VendingMachineItem("Gold", -10.0);
	        fail("Expected VendingMachineException was not thrown");
	    } catch (VendingMachineException e) {
	        assertEquals("Price cannot be less than zero", e.getMessage());
	    }
	    
	    try {
	        new VendingMachineItem("Silver", -5.0);
	        fail("Expected VendingMachineException was not thrown");
	    } catch (VendingMachineException e) {
	        assertEquals("Price cannot be less than zero", e.getMessage());
	    }
	}

	//Tests getting the name of a VendingMachineItem.
	@Test
	public void testGetName() 
	{
		assertEquals("Cheesy Doodles", VendingItem.getName());
	}

	//Tests getting the price of a VendingMachineItem.
	@Test
	public void testGetPrice() 
	{
		assertEquals(15.0, VendingItem.getPrice(), 0.001);
	}

}
