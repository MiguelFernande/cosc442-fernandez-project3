package edu.towson.cis.cosc442.project2.vendingmachine;

import org.junit.Test;
import static org.junit.Assert.*;

public class VendingMachineExceptionTest 
{

    // Test the default constructor
    @Test
    public void testDefaultConstructor() 
    {
        VendingMachineException exception = new VendingMachineException();
        assertNotNull(exception);
        assertNull(exception.getMessage());
    }

    // Test the constructor with an error message
    @Test
    public void testConstructorWithMessage() 
    {
        String errorMessage = "An error occurred";
        VendingMachineException exception = new VendingMachineException(errorMessage);
        assertNotNull(exception);
        assertEquals(errorMessage, exception.getMessage());
    }

    // Test that VendingMachineException is a subclass of RuntimeException
    @Test
    public void testInheritance()
    {
        VendingMachineException exception = new VendingMachineException();
        assertTrue(exception instanceof RuntimeException);
    }
}